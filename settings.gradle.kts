rootProject.name = "open-link-tracker"
include(
    "open-link-tracker-core-module",
    "open-link-tracker-api-module",
    "open-link-tracker-gui-module",
    "open-link-tracker-application"
)

dependencyResolutionManagement {
    versionCatalogs {
        val kotlinVersion = "2.0.0"
        val ktorVersion = "3.0.0-beta-1"
        val exposedVersion = "0.51.1"
        val flywayVersion = "10.15.2"
        val logbackVersion = "1.4.14"
        create("libs") {
            library("config-parameters", "com.github.ajalt.clikt:clikt:4.4.0")

            library("logging-backend", "ch.qos.logback:logback-classic:$logbackVersion")

            library("server-serialization-core", "org.jetbrains.kotlinx:kotlinx-serialization-core:1.7.0")
            library("server-core", "io.ktor:ktor-server-core-jvm:$ktorVersion")
            library("server-engine", "io.ktor:ktor-server-netty-jvm:$ktorVersion")
            library("server-statusPages", "io.ktor:ktor-server-status-pages:$ktorVersion")
            library("server-contentNegotiation", "io.ktor:ktor-server-content-negotiation:$ktorVersion")
            library("server-templating-html", "io.ktor:ktor-server-html-builder:$ktorVersion")
            library("server-serialization-json", "io.ktor:ktor-serialization-kotlinx-json:$ktorVersion")

            library("persistence-orm-core", "org.jetbrains.exposed:exposed-core:$exposedVersion")
            library("persistence-orm-jdbc", "org.jetbrains.exposed:exposed-jdbc:$exposedVersion")
            library("persistence-orm-dao", "org.jetbrains.exposed:exposed-dao:$exposedVersion")
            bundle("persistence-orm", listOf("persistence-orm-core", "persistence-orm-jdbc", "persistence-orm-dao"))
            library("persistence-database-inMemory", "com.h2database:h2:2.2.224")
            library("persistence-migrationTool", "org.flywaydb:flyway-core:$flywayVersion")
            library("persistence-migrationToolPgAdapter", "org.flywaydb:flyway-database-postgresql:$flywayVersion")
            bundle("persistence-migrationTool", listOf("persistence-migrationTool", "persistence-migrationToolPgAdapter"))
            library("persistence-jdbcDriver", "org.postgresql:postgresql:42.7.3")
        }
        create("testLibs") {
            library("unitTests-kotlin", "org.jetbrains.kotlin:kotlin-test-junit:$kotlinVersion")
            library("unitTests-junit", "org.junit.jupiter:junit-jupiter:5.10.3")
            bundle("unitTests", listOf("unitTests-kotlin", "unitTests-junit"))
            library("server", "io.ktor:ktor-server-tests-jvm:$ktorVersion")
        }
    }
}