package cc.open_link_tracker.modules.core.model

import cc.open_link_tracker.modules.core.defaultBasePath
import cc.open_link_tracker.modules.core.defaultCharSet
import cc.open_link_tracker.modules.core.stabilizePath
import cc.open_link_tracker.modules.core.toBaseNEncoded
import org.jetbrains.exposed.dao.LongEntity
import org.jetbrains.exposed.dao.LongEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.LongIdTable
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.transactions.transaction

object TrackingLinkTable : LongIdTable(name = "tracking_links") {
    val url = varchar("url", 8192)
    val name = varchar("name", 100).nullable()
    val description = varchar("description", 250).nullable()
    val permanentRedirect = bool("permanent_redirect").default(false)
    val ownerId = varchar("owner_id", 50)
}

class TrackingLinkDao(id: EntityID<Long>) : LongEntity(id) {
    companion object : LongEntityClass<TrackingLinkDao>(TrackingLinkTable)

    var url by TrackingLinkTable.url
    var name by TrackingLinkTable.name
    var description by TrackingLinkTable.description
    var permanentRedirect by TrackingLinkTable.permanentRedirect
    var ownerId by TrackingLinkTable.ownerId

    fun toTrackingLinkDto() = TrackingLinkDto(id.value, url, ownerId, name, description, permanentRedirect)
}

data class TrackingLinkDto(
    val id: Long,
    val url: String,
    val ownerId: String,
    val name: String? = null,
    val description: String? = null,
    val permanentRedirect: Boolean = false
) {
    fun trackingUrl(
        host: String,
        basePath: String = defaultBasePath,
        https: Boolean = true,
        charSet: List<Char> = defaultCharSet
    ): String {
        val protocol = if (https) "https" else "http"
        return protocol + "://" + host + basePath.stabilizePath() + "/" + id.toBaseNEncoded(charSet)
    }
}

data class TrackingLinkCreateDto(
    val url: String,
    val name: String? = null,
    val description: String? = null,
    val permanentRedirect: Boolean = false
)

object TrackingLinkService {
    fun createTrackingLink(dto: TrackingLinkCreateDto, ownerId: String): TrackingLinkDto = transaction {
        val link = TrackingLinkDao.new {
            url = dto.url
            name = dto.name
            description = dto.description
            permanentRedirect = dto.permanentRedirect
            this.ownerId = ownerId
        }

        link.toTrackingLinkDto()
    }

    fun findById(id: Long): TrackingLinkDto? = transaction {
        TrackingLinkDao.findById(id)?.toTrackingLinkDto()
    }

    fun findByNameAndOwner(name: String, owner: String): List<TrackingLinkDto> = transaction {
        TrackingLinkDao.find { TrackingLinkTable.name eq name and (TrackingLinkTable.ownerId eq owner) }
            .map { it.toTrackingLinkDto() }
    }
}