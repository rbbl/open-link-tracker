package cc.open_link_tracker.modules.core

val defaultCharSet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-".toList()

fun Long.toBaseNEncoded(charSet: List<Char> = defaultCharSet): String {
    if (this == 0L) return charSet[0].toString()

    val digits = mutableListOf<Int>()
    var num = this
    while (num > 0) {
        digits.add((num % charSet.size).toInt())
        num /= charSet.size
    }
    digits.reverse()
    return digits.fold("") { acc, i -> acc + charSet[i] }
}

fun String.decodeBaseN(charSet: List<Char> = defaultCharSet): Long {
    var i = 0L
    this.forEach {
        i = (i * charSet.size) + charSet.indexOf(it)
    }
    return i
}

fun String.stabilizePath (): String{
    return if(this.startsWith("/")){
        this
    }else{
        "/$this"
    }
}