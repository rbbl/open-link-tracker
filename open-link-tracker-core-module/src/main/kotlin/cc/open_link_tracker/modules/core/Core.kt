package cc.open_link_tracker.modules.core

import cc.open_link_tracker.modules.core.model.TrackingLinkCreateDto
import cc.open_link_tracker.modules.core.model.TrackingLinkService
import io.ktor.server.application.Application
import io.ktor.server.plugins.NotFoundException
import io.ktor.server.response.respondRedirect
import io.ktor.server.response.respondText
import io.ktor.server.routing.Route
import io.ktor.server.routing.get
import io.ktor.server.routing.route
import io.ktor.server.routing.routing
import org.flywaydb.core.Flyway
import org.jetbrains.exposed.sql.Database

private const val defaultJdbcDriver = "org.postgresql.Driver"
const val defaultSystemUserIdentifier = "system"
const val defaultBasePath = "/l"

fun Application.linkTrackerCore(
    jdbcUrl: String,
    dbUser: String,
    dbPassword: String,
    systemTrackingLinks: Set<TrackingLinkCreateDto>? = null,
    basePath: String = defaultBasePath,
    jdbcDriver: String = defaultJdbcDriver,
    systemUserIdentifier: String = defaultSystemUserIdentifier
) {
    routing {
        route(basePath) {
            linkTrackerCore(jdbcUrl, dbUser, dbPassword, systemTrackingLinks, jdbcDriver, systemUserIdentifier)
        }
    }
}

fun Route.linkTrackerCore(
    jdbcUrl: String,
    dbUser: String,
    dbPassword: String,
    systemTrackingLinks: Set<TrackingLinkCreateDto>? = null,
    jdbcDriver: String = defaultJdbcDriver,
    systemUserIdentifier: String = defaultSystemUserIdentifier
) {
    Database.connect(jdbcUrl, jdbcDriver, dbUser, dbPassword)
    handleDbMigration(jdbcUrl, dbUser, dbPassword)
    addSystemLinks(systemTrackingLinks, systemUserIdentifier)
    get {
        call.respondText("Hello Core!")
    }
    get("/{encodedId}") {
        call.parameters["encodedId"]?.decodeBaseN().let {
            if (it == null) throw NotFoundException()
            val result = TrackingLinkService.findById(it) ?: throw NotFoundException()
            call.respondRedirect(result.url, result.permanentRedirect)
        }
    }
}

private fun handleDbMigration(jdbcUrl: String, dbUser: String, dbPassword: String) {
    val flyway = Flyway.configure().dataSource(
        jdbcUrl,
        dbUser,
        dbPassword
    ).load()
    flyway.migrate()
}

private fun addSystemLinks(
    links: Set<TrackingLinkCreateDto>?,
    systemUserIdentifier: String = defaultSystemUserIdentifier
) {
    if (links == null) return

    links.forEach {
        if(it.name != null && TrackingLinkService.findByNameAndOwner(it.name, systemUserIdentifier).isNotEmpty()){
            return@forEach
        }
        TrackingLinkService.createTrackingLink(
            it,
            systemUserIdentifier
        )
    }
}
