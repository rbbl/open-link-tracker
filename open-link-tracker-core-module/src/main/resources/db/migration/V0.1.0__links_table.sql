CREATE TABLE tracking_links
(
    id                 bigserial PRIMARY KEY,
    name               varchar(100),
    description        varchar(250),
    url                varchar(8192),
    permanent_redirect boolean DEFAULT false,
    owner_id           varchar(50)
);

CREATE TABLE tracking_link_permissions
(
    tracking_link_id bigint,
    user_id          varchar(50),
    permissions      bigint,
    CONSTRAINT pk PRIMARY KEY (tracking_link_id, user_id),
    CONSTRAINT link_id_fk FOREIGN KEY (tracking_link_id) references tracking_links (id)
);
