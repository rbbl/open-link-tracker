package cc.open_link_tracker.modules.core

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

class UtilsKtTest {
    @Test
    fun testConsistency() {
        for(i in 0..1000L){
            assertEquals(i, i.toBaseNEncoded().decodeBaseN())
        }
    }
}