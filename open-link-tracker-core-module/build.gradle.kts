val kotlinVersion: String by project
val ktorVersion: String by project
val exposedVersion: String by project

plugins {
    kotlin("jvm")
}

group = "cc.rbbl"
version = "0.0.1"


repositories {
    mavenCentral()
}

dependencies {
    implementation(libs.server.core)

    implementation(libs.persistence.jdbcDriver)
    implementation(libs.bundles.persistence.migrationTool)
    implementation(libs.bundles.persistence.orm)

    testImplementation(testLibs.server)
    testImplementation(testLibs.bundles.unitTests)
}

tasks.test {
    useJUnitPlatform()
}
