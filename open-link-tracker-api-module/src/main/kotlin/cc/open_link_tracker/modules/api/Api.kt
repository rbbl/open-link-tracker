package cc.open_link_tracker.modules.api

import io.ktor.server.application.Application
import io.ktor.server.response.respondText
import io.ktor.server.routing.Route
import io.ktor.server.routing.get
import io.ktor.server.routing.route
import io.ktor.server.routing.routing

fun Application.linkTrackerApi(basePath: String = "/api") {
    routing {
        route(basePath) {
            linkTrackerApi()
        }
    }
}

fun Route.linkTrackerApi() {
    get {
        call.respondText("Hello API!")
    }
}
