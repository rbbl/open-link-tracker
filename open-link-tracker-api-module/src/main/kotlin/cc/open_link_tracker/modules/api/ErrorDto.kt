package cc.open_link_tracker.modules.api

import kotlinx.serialization.Serializable

@Serializable
data class ErrorDto(val code: Int, val reason:String, val message: String = reason)
