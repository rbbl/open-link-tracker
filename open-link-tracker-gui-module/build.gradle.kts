val kotlinVersion: String by project
val ktorVersion: String by project

plugins {
    kotlin("jvm")
    kotlin("plugin.serialization") version "2.0.0"
}

group = "cc.rbbl"
version = "0.0.1"


repositories {
    mavenCentral()
}

dependencies {
    implementation(libs.server.core)
    implementation(libs.server.serialization.core)
    testImplementation(testLibs.server)
    testImplementation(testLibs.bundles.unitTests)
}

tasks.test {
    useJUnitPlatform()
}
