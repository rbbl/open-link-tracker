package cc.open_link_tracker.modules.gui

import io.ktor.server.application.Application
import io.ktor.server.response.respondText
import io.ktor.server.routing.Route
import io.ktor.server.routing.get
import io.ktor.server.routing.route
import io.ktor.server.routing.routing

fun Application.linkTrackerGui(basePath: String = "/gui") {
    routing {
        route(basePath) {
            linkTrackerGui()
        }
    }
}

fun Route.linkTrackerGui() {
    get {
        call.respondText("Hello GUI!")
    }
}