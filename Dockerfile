FROM docker.io/gradle:8.7.0-jdk21 as Builder
COPY . /app/
ARG CI_COMMIT_TAG
ENV CI_COMMIT_TAG $CI_COMMIT_TAG
WORKDIR /app
RUN gradle open-link-tracker-application:installDist

FROM docker.io/eclipse-temurin:21.0.3_9-jdk
COPY --from=Builder /app/open-link-tracker-application/build/install/open-link-tracker-application /open-link-tracker-application
ENTRYPOINT ["/open-link-tracker-application/bin/open-link-tracker-application"]