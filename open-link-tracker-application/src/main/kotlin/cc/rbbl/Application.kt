package cc.rbbl

import cc.open_link_tracker.modules.api.ErrorDto
import cc.open_link_tracker.modules.api.linkTrackerApi
import cc.open_link_tracker.modules.core.linkTrackerCore
import cc.open_link_tracker.modules.core.model.TrackingLinkCreateDto
import cc.open_link_tracker.modules.core.model.TrackingLinkService
import cc.open_link_tracker.modules.gui.linkTrackerGui
import io.ktor.http.HttpStatusCode
import io.ktor.serialization.kotlinx.json.json
import io.ktor.server.application.Application
import io.ktor.server.application.install
import io.ktor.server.engine.embeddedServer
import io.ktor.server.html.respondHtml
import io.ktor.server.netty.Netty
import io.ktor.server.plugins.NotFoundException
import io.ktor.server.plugins.contentnegotiation.ContentNegotiation
import io.ktor.server.plugins.statuspages.StatusPages
import io.ktor.server.request.path
import io.ktor.server.response.respond
import io.ktor.server.routing.get
import io.ktor.server.routing.routing
import kotlinx.html.a
import kotlinx.html.body
import kotlinx.html.br
import kotlinx.html.h1
import kotlinx.html.head
import kotlinx.html.span
import kotlinx.html.title

fun main(args: Array<String>) {
    val config = ProgramConfig()
    config.main(args)
    embeddedServer(Netty, port = config.port, host = config.hostBind) {
        module(config)
    }.start(wait = true)
}

fun Application.module(config: ProgramConfig) {
    install(ContentNegotiation) {
        json()
    }
    val defaultLinks = setOf(
        TrackingLinkCreateDto("https://www.rbbl.cc/", "test_temp"),
        TrackingLinkCreateDto("https://www.rbbl.cc/", "test_perm", permanentRedirect = true)
    )
    linkTrackerCore(config.jdbcUrl, config.dbUser, config.dbPassword, defaultLinks)
    statusPages()
    linkTrackerApi()
    linkTrackerGui()
    installRouting(config)
}

fun Application.statusPages() {
    install(StatusPages) {
        status(HttpStatusCode.NotFound) { code ->
            throw NotFoundException()
        }
        exception<NotFoundException> { call, cause ->
            call.response.status(HttpStatusCode.NotFound)
            if (call.request.path().startsWith("/api")) {
                call.respond(
                    ErrorDto(
                        HttpStatusCode.NotFound.value, HttpStatusCode.NotFound.description, cause.message ?: ""
                    )
                )
            } else {
                call.respondHtml {
                    head {
                        title { +"Error: Not Found" }
                    }
                    body {
                        h1 {
                            +"Error: Not Found"
                        }
                    }
                }
            }
        }
    }
}

fun Application.installRouting(config: ProgramConfig) {
    routing {
        get("/") {
            val tempLink = TrackingLinkService.findByNameAndOwner("test_temp", "system").first()
            val permLink = TrackingLinkService.findByNameAndOwner("test_perm", "system").first()
            call.respondHtml {
                body {
                    span {
                        a {
                            href = tempLink.trackingUrl(config.redirectHost, https = false)
                            +"temp_link"
                        }
                    }
                    br {}
                    span {
                        a {
                            href = permLink.trackingUrl(config.redirectHost, https = false)
                            +"perm_link"
                        }
                    }
                }
            }
        }
    }
}