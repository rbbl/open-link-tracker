package cc.rbbl

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.options.default
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.types.int

class ProgramConfig : CliktCommand() {
    val jdbcUrl by option(envvar = "JDBC_URL").default("jdbc:h2:mem:link_tracking;MODE=PostgreSQL;DATABASE_TO_LOWER=TRUE;DEFAULT_NULL_ORDERING=HIGH")
    val dbUser by option(envvar = "DB_USER").default("root")
    val dbPassword by option(envvar = "DB_PASSWORD").default("")
    val port by option(envvar = "PORT").int().default(8080)
    val redirectHost by option(envvar = "REDIRECT_HOST").default("localhost:8080")
    val hostBind by option(envvar = "HOST_BIND_ADDRESS").default("0.0.0.0")

    override fun run() {
    }
}