val kotlinVersion: String by project
val logbackVersion: String by project

plugins {
    kotlin("jvm")
    id("io.ktor.plugin") version "3.0.0-beta-1"
}

group = "cc.rbbl"
version = "0.0.1"

repositories {
    mavenCentral()
}

application {
    mainClass.set("cc.rbbl.ApplicationKt")
    applicationDefaultJvmArgs = listOf("-Dio.ktor.development=true")
}

dependencies {
    implementation(project(":open-link-tracker-core-module"))
    implementation(project(":open-link-tracker-api-module"))
    implementation(project(":open-link-tracker-gui-module"))

    implementation(libs.config.parameters)
    implementation(libs.persistence.database.inMemory)

    implementation(libs.server.core)
    implementation(libs.server.engine)
    implementation(libs.server.templating.html)
    implementation(libs.server.contentNegotiation)
    implementation(libs.server.serialization.json)
    implementation(libs.server.statusPages)
    implementation(libs.logging.backend)

    testImplementation(testLibs.server)
    testImplementation(testLibs.bundles.unitTests)
}

tasks.test {
    useJUnitPlatform()
}
