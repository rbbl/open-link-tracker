val kotlinVersion: String by project

group = "cc.rbbl"

plugins {
    kotlin("jvm") version "2.0.0" apply false
    id("idea")
}

idea {
    module{
        isDownloadSources = false
        isDownloadJavadoc = true
    }
}

repositories {
    mavenCentral()
}